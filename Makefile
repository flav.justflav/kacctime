name = $(shell uname -m)-LNX$(shell uname -r | sed -re 's/([0-9]).([0-9]).*/\1\2/')
obj-m += kacctime_$(name).o
ccflags-y += -Wall
EXTRA_CFLAGS += $(CFLAGS_EXTRA)

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

test:
	cc -m64 test_kacctime.c -o test_kacctime_64
	cc -m32 test_kacctime.c -o test_kacctime_32
	cc -m64 test_clock_gettime_mono.c -o test_clock_gettime_mono_64
